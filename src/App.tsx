import React, { createContext, lazy, Suspense, useEffect, useState } from 'react';
import create from 'zustand'
import { Demo1Page } from './pages/demo1/Demo1Page';
import { Demo2State } from './pages/demo2/Demo2State';
import { Interceptor } from './core/Interceptor';
import { BrowserRouter, Routes, Route, Navigate, Link } from 'react-router-dom';
import { NavBar } from './core/NavBar';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { CatalogDetails } from './pages/catalog-details/CatalogDetails';
import { Login } from './pages/login/Login';
import { Registration } from './pages/login/components/Registration';
import { SignIn } from './pages/login/components/SignIn';
import { LostPass } from './pages/login/components/LostPass';
import { ContextDemo } from './pages/context/ContextDemo';

const ProductsPage = lazy(() => import('./pages/products/ProductsPage'))

interface AppGlobalState {
  name: string;
  token: string;
  updateName: (newName: string) => void;
}


export const useStore = create<AppGlobalState>(set => ({
  name: '',
  token: '',
  updateName: (newName: string) => {
    set(s => ({...s, name: newName}))
  }
}))


export const AppContext = createContext<{auth: any} | null>(null)
function App(props: any) {
  const [auth, setAuth] = useState<{auth: any} | null>(null);

  return (
    <AppContext.Provider value={auth}>
      <BrowserRouter>
       <NavBar />

        <hr/>
        <Interceptor />
        <Suspense fallback={<div>loading....</div>}>
          <Routes>
            <Route path="products" element={<ProductsPage />} />
            <Route path="catalog" element={<CatalogPage />} />
            <Route path="catalog/:postId" element={<CatalogDetails />} />
            <Route path="demo2" element={<Demo2State />} />
            <Route path="demo1" element={<Demo1Page />} />
            <Route path="context" element={<ContextDemo />} />
            <Route path="login" element={<Login />}>
              <Route path="registration" element={<Registration />} />
              <Route path="lostpass" element={<LostPass />} />
              <Route path="signin" element={<SignIn onLogin={setAuth} />} />
              <Route index element={<Navigate to="signin" />} />
            </Route>

            <Route path="" element={
              <Navigate to="demo1" />
            } />
            <Route path="*" element={
              <div>Page not found</div>
            } />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </AppContext.Provider>
  )
}
export default App;

