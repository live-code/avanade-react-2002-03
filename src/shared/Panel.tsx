import React from 'react';
import cn from 'classnames';

interface PanelProps {
  title: string;
  type?: PanelType;
  icon?: string;
  marginBottom?: boolean;
  onIconClick?: (val: number) => void
}

type PanelType =  'success' | 'failed';


export const Panel: React.FC<PanelProps> = (props) => {
  const { icon, type, title, children, onIconClick, marginBottom } = props;

  function iconClickHandler() {
    if (onIconClick) {
      onIconClick(Math.random())
    }
  }
  return (
    <div
      className={cn('card', { 'mb-2': marginBottom })}
    >
      <div
        className={cn('card-header',
          { 'bg-success': type === 'success' },
          { 'bg-danger': type === 'failed' },
        )}
      >
        {title}
        {
          props.icon && <div className="pull-right">
            <i className={icon} onClick={iconClickHandler}/>
          </div>
        }
      </div>
      <div className="card-body">{children}</div>
    </div>
  )
}
