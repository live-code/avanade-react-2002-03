interface TabBarProps {
  items: any[];
  onTabClick: (item: any) => void;
}

export const TabBar: React.VFC<TabBarProps> = (props) => {
  return (
    <ul className="nav nav-tabs">
      {
        props.items.map(item => {
          return (
            <li
              className="nav-item" key={item.id}
              onClick={() => props.onTabClick(item)}
            >
              <a className="nav-link active">{item.name}</a>
            </li>
          )
        })
      }
    </ul>
  )
}
