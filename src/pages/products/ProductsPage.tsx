import React, { useEffect } from 'react';
import { Products } from './components/Products';
import { useProducts, useXYZ } from './hooks/useProducts';


export default function ProductsPage() {
  const { products, doSomething, data } = useProducts();

  return (
    <>
      <div>
        {
          products.length ?
            <Products items={products}/> :
            <Empty />
        }

        <button onClick={doSomething}>DO SOMETHING</button>
      </div>
    </>
  )
}


const Empty = () => <h1>Non c'è nulla!!! </h1>;

// const Example = () => React.createElement(Empty, { title: 'ciao' }, <h1>ciao</h1>)
