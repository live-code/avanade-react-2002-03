import { useEffect, useState } from 'react';
import { Product } from '../model/product';
import axios from 'axios';

export function useProducts() {
  const [products, setProducts] = useState<Product[]>([]);
  const { data } = useXYZ(products)

  console.log(data)
  useEffect(() => {
    axios.get<Product[]>('http://localhost:3001/catalog')
      .then(res => {
        setProducts(res.data)
      })
  }, [])

  function doSomething() {
    console.log('do something')
  }

  return {
    products,
    doSomething,
    data: axios.get('')
  }
}

export function useXYZ(value: any[]) {
  return {
    data: 'length' + value.length
  }
}
