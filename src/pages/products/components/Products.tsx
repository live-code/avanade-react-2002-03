import React from 'react';
import { Product } from '../model/product';

interface ProductsProps {
  title?: string;
  items: Product[]
}

// export function Products(props: ProductsProps) {
// export const Products = (props: ProductsProps) => {
export const Products: React.VFC<ProductsProps> = (props) => {
  function openUrl(value: string) {
    console.log('url', value)
  }

  return (
    <div>
      { props.title && <h1>{props.title}</h1>}

      <ul>
        {
          props.items.map(item => {
            return (
              <div key={item.id} className="Demo1">
                {item.name}
                <button onClick={() => openUrl(item.url)}>Visit</button>
              </div>
            )
          })
        }

      </ul>
    </div>
  )
}
/*

document.getElementById('btn').addEventListener('click', openUrl)
document.getElementById('btn').addEventListener('click', function() {
  openUrl(123)
})
*/
