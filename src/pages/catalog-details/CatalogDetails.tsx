import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Post } from '../../model/post';
import { Link, useParams } from 'react-router-dom';

export const CatalogDetails: React.FC = () => {
  const [post, setPost] = useState<Post | null >(null)
  const params = useParams()


  useEffect(() => {
    axios.get<Post>('https://jsonplaceholder.typicode.com/posts/' + params.postId )
      .then(res => {
        setPost(res.data);
      })
  }, [params.postId])

  function getNextLink() {
    if (params.postId) {
      const nextId = +params.postId + 1;
      return '/catalog/' + nextId
    }
    return '';
  }
  return <div>
    <h1>CatalogDetails</h1>

    {post?.title}

    <hr/>
    <Link to={getNextLink()}>next</Link>

  </div>
};
