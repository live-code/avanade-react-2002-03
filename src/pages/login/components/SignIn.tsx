import React  from 'react';
import { useStore } from '../../../App';

interface SignInProps {
  onLogin: (auth: any) => void;
}
export const SignIn: React.FC<SignInProps> = (props) => {
  const updateName = useStore(s => s.updateName);

  return <div>SignIn
    <input type="text"/>
    <input type="text"/>
    <button onClick={() => props.onLogin({ auth: { name: 'Fabio'}})}>LOGIN</button>
    <button onClick={() => updateName('pippo')}>LOGIN pippo</button>
  </div>
};
