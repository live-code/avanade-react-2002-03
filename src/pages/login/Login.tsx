import React  from 'react';
import { LostPass } from './components/LostPass';
import { Registration } from './components/Registration';
import { SignIn } from './components/SignIn';
import { Link, Outlet, useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { useStore } from '../../App';

export const Login: React.FC = () => {
  const navigate = useNavigate();

  const state = useStore(s => s.name)

  console.log('login')
  function goto() {
      navigate('/catalog')
  }

  return <div>
    <h1>Login {state}</h1>

    <Outlet />


    <hr/>
    <Link to="signin">SignIn </Link> -
    <Link to="registration">registration</Link> -
    <Link to="lostpass">lostpass</Link>
    <button onClick={goto}>goto</button>
  </div>
};
