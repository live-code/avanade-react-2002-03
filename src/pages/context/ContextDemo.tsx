import React, { useState, createContext, useContext } from 'react';

interface PageState {
  count: number;
  random: number;
}

const DataContext = createContext<PageState | null>(null);

export function ContextDemo() {
  console.log('------\nApp: render')
  const [value, setValue] = useState<PageState>({ count: 0, random: 1.42523})

  function inc() {
    // setValue({...value, count: value.count + 1 })
    setValue(s => ({...s, count: s.count + 1 }))
  }

  return (
    <DataContext.Provider value={value}>
      <h3>Demo Context</h3>
      <button onClick={inc}>+</button>
      <button>-</button>
      <button>Random</button>
      <Dashboard inc={inc} />
    </DataContext.Provider>
  );
}

const Dashboard = React.memo((props: any) => {
  console.log(' dashboard: render')
  return <div className="comp">
    <CounterPanel />
    <RandomPanel inc={props.inc} />
  </div>
})

const CounterPanel = () => {
  console.log('  CounterPanel: render')
  const state = useContext(DataContext)
  return <div className="comp">
    Count: {state?.count}
  </div>
}


const RandomPanel = React.memo((props: any) => {
  console.log('  Random Panel: render')
  const state = useContext(DataContext)

  return <div className="comp">
    RandomPanel: {state?.random}
    <button onClick={props.inc}>update count</button>
  </div>
})
