import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Post } from '../../model/post';
import { Link } from 'react-router-dom';

export const CatalogPage: React.FC = () => {
  const [posts, setPosts] = useState<Post[]>([])

  useEffect(() => {
    axios.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .then(res => {
        setPosts(res.data);
      })
  }, [])

  return <div>

    {
      posts.map((post, index) => {
        return (<li key={index}>
          <Link to={'/catalog/' + post.id}>
            {post.title}
          </Link>
        </li>)
      })
    }

  </div>
};
