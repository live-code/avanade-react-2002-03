import { useEffect, useState } from 'react';
import axios from 'axios';


export interface User {
  id: number;
  name: string;
}

const initialState = { pending: false, users: [] };


export function useUsers() {
  const [data, setData] = useState<{pending: boolean, users: User[]}>(initialState)
  const [activeUser, setActiveUser] = useState<User | null>(null)
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    setData(d => ({ ...d, pending: true }));
    const controller = new AbortController();

    axios.get<User[]>('http://localhost:3001/users', {
      signal: controller.signal
    })
      .then(function (res) {
        setData({
          pending: false,
          users: res.data
        })
      })
      .catch(err => {
        setError(true);
        setData({users: [], pending: false});
      })

    //  cleanUp
    return () => {
      controller.abort()
    }
  }, [])

  function saveHandler(formData: Partial<User>) {
    if (activeUser?.id) {
      editHandler(formData)
    } else {
      addHandler(formData)
    }
  }

  function editHandler(formData: Partial<User>) {
    setData({ ...data, pending: true });
    setError(false);
    axios.patch<User>('http://localhost:3001/users/' + activeUser?.id, formData)
      .then(res => {
        setData({
          pending: false,
          users: data.users.map(u => u.id === activeUser?.id ? {...u, ...formData} : u)
        })
        setError(false);
      })
      .catch(() => {
        setData({...data, pending: false})
        setError(true);
      })

  }

  function addHandler(formData: Partial<User>) {
    setData({ ...data, pending: true });
    setError(false);
    axios.post<User>('http://localhost:3001/users', formData)
      .then(res => {
        setData({
          pending: false,
          users: [...data.users, res.data]
        })
        setError(false);
      })
      .catch(() => {
        setData({...data, pending: false})
        setError(true);
      })

  }

  function deleteUser(id: number) {
    setData({ ...data, pending: true });
    fetch('http://localhost:3001/users/' + id)
      .then(res => res.json())
      .then(res => res.data)
      .catch()

    axios.delete('http://localhost:3001/users/' + id)
      .then(function () {
        setData({
          pending: false,
          users: data.users.filter(u => u.id !== id )
        })
        if (id === activeUser?.id) {
          setActiveUser(null);
        }
      })
  }

  function setActive(user: User | null) {
    setActiveUser(user)
  }

  return {
    data,
    error,
    activeUser,
    actions: {
      save: saveHandler,
      setActive,
      deleteUser
    }
  }
}
