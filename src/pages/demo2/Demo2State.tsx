import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { List } from './components/UserList';
import { UserFormControlled } from './components/UserFormControlled';
import { User, useUsers } from './hooks/useUsers';

export const Demo2State: React.FC = () => {
  const { data, error , activeUser, actions } = useUsers();

  return <div>
    { data.pending && <div>loader.....</div>}
    { error && <div>errore lato server</div> }

    <UserFormControlled
      user={activeUser}
      onSave={actions.save}
      onClean={() => actions.setActive(null)}
    />

    <List items={data.users}
          activeUser={activeUser}
          onItemClick={user => actions.setActive(user)}
          onDeleteUser={actions.deleteUser} />

    { activeUser && <UserDetails user={activeUser}/>}
  </div>
};








// =====
const UserDetails: React.VFC<{ user: User}> = props => {
  const [user, setUser] = useState<any | null>(null)

  useEffect(() => {
    axios.get<User>('http://localhost:3001/users/' + props.user.id)
      .then(res => {
        setUser(res.data)
      })
  }, [props.user])

  return <h1>
    Scheda utente
    { user && <div>{user.id}</div>}
    <div>{user?.name}</div>
    <div>{user?.surname}</div>
    <div>{user?.email}</div>
  </h1>
}
