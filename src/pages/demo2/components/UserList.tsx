import React, { useState } from 'react';
import { User } from '../../../model/user';
import classNames from 'classnames';
import { UserListItem } from './UserListItem';

interface ListProps {
  activeUser: User | null;
  items: User[]
  onItemClick: (user: User) => void;
  onDeleteUser: (id: number) => void;
}

export const List: React.VFC<ListProps> = (props) => {
  const { items, onDeleteUser, onItemClick, activeUser} = props;


  return <div>
    {
      items.map(u => {
        return <UserListItem
          item={u}
          activeUser={activeUser}
          onItemClick={onItemClick}
          onDeleteUser={onDeleteUser}
          key={u.id}
        />
      })
    }
  </div>
}
