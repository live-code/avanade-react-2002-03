import React, { useState } from 'react';
import classNames from 'classnames';
import { User } from '../../../model/user';

interface UserListItemProps {
  activeUser: User | null;
  item: User;
  onItemClick: (user: User) => void;
  onDeleteUser: (id: number) => void;
}
export const UserListItem: React.VFC<UserListItemProps> = ({ item, onDeleteUser, onItemClick, activeUser }) => {
  const [open, setOpen] = useState<boolean>(false)

  function deleteHandler(event: React.MouseEvent, id: number) {
    event.stopPropagation();
    onDeleteUser(id)
  }

  function showHandler(e: React.MouseEvent) {
    e.stopPropagation();
    setOpen(s => !s);
  }

  return (
    <li
      onClick={() => onItemClick(item)}
      className={classNames('list-group-item', { active: item.id === activeUser?.id})}
    >
      {item.name} - {item.username}
      <button onClick={showHandler}>SHOW</button>
      <button onClick={(e) => deleteHandler(e, item.id)}>DEL</button>

      {
        open &&
        <div>
          {item.email}
          {item.phone}
        </div>
      }
    </li>
  )
};
