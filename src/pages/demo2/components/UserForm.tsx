import React from 'react';

interface UserFormProps {
  onAdd: (text: string) => void;
}
export const UserForm: React.VFC<UserFormProps> = (props) => {

  function addHandler(event: React.KeyboardEvent) {
    const input: HTMLInputElement = (event.target as HTMLInputElement);
    if (event.key === 'Enter' && input.value.length > 3) {
      props.onAdd(input.value)
    }
  }

  return (
    <div>
      <input type="text" onKeyDown={addHandler}/>
    </div>
  )
}
