import React, { useEffect, useRef, useState } from 'react';

interface UserFormProps {
  onAdd: (name: string, username: string) => void;
}
export const UserFormUncontrolled: React.VFC<UserFormProps> = (props) => {
  const [error, setError] = useState<boolean>(false);
  const inputName = useRef<HTMLInputElement>(null);
  const inputUserName = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputName.current?.focus();
  }, [])

  function save() {
    const name = inputName.current;
    const username = inputUserName.current;
    if (
      name && username &&
      name.value.length > 3
    ) {
      console.log(inputName.current?.value)
      console.log(inputUserName.current?.value)
      props.onAdd(name.value, username.value)
      setError(false)
    } else {
      setError(true)
    }

  }

  return (
    <div>
      { error && <div>Form errato</div>}
      <input ref={inputName} type="text" placeholder="add name"/>
      <input ref={inputUserName} type="text" placeholder="add username"/>

      <button onClick={save}>ADD</button>
    </div>
  )
}
