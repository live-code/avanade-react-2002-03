import React, { useEffect, useRef, useState } from 'react';
import cn from 'classnames';
import { User } from '../../../model/user';

interface UserFormProps {
  user: User | null;
  onClean: () => void;
  onSave: (formData: Partial<User>) => void;
}

const initialState = { name: '', username: '', gender: ''};
type Key = keyof User;

export const UserFormControlled: React.VFC<UserFormProps> = (props) => {
  const [formData, setFormData] = useState<Partial<User>>(initialState)

  useEffect(() => {
    if (props.user) {
      setFormData({gender: '', ...props.user})
    } else {
      setFormData(initialState)
    }
  }, [props.user])

  function save(e: React.FormEvent) {
    e.preventDefault();
    props.onSave(formData)
  }

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }


  const isNameValid = formData.name && formData.name.length > 3;
  const isUsernameValid = formData.username;
  const isGenderValid = formData.gender;
  const valid = isNameValid && isUsernameValid && isGenderValid;

  const keys: Key[] = Object.keys(formData) as Key[]

  return (
    <form onSubmit={save}>
      {
        /*
        // Creazione dinamica del form in base alle prop dell'oggetto
        keys.map(key => {
          const value: string = formData[key] as Key;
          return <div key={key}>
            <input type="text" name={key} placeholder={key} value={value} onChange={onChangeHandler}/>
          </div>
        })
        */
      }

      {!valid && <div>form errors</div>}
      <input
        className={cn('form-control', {'is-invalid': !isNameValid, 'is-valid': isNameValid} )}
        type="text" name="name" placeholder="name" value={formData.name} onChange={onChangeHandler}/>
      <input
        className={cn('form-control', {'is-invalid': !isUsernameValid, 'is-valid': isUsernameValid} )}
        type="text" name="username" placeholder="username" value={formData.username} onChange={onChangeHandler}/>

      <select
        name="gender" value={formData.gender} onChange={onChangeHandler}
        className={cn('form-control', {'is-invalid': !isGenderValid, 'is-valid': isGenderValid} )}
      >
        <option value=''>Select Gender</option>
        <option value="M">Male</option>
        <option value="F">Female</option>
      </select>

      <button disabled={!valid} type="submit">
        {formData.id ? 'EDIT' : 'ADD'}
      </button>
      <button type="button" onClick={props.onClean}>add another</button>
    </form>
  )
}
