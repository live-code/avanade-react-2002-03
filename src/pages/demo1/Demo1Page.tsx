import React from 'react';
import { Panel } from '../../shared/Panel';
import { TabBar } from '../../shared/TabBar';
import { User } from '../../model/user';

const users: User[] = [
  { id: 1, name: 'Fabio'},
  { id: 2, name: 'Pippo'},
  { id: 3, name: 'Pluto'},
]

export function Demo1Page() {
  const openUrl = (url: string) => window.open(url);
  const doSomething = (val: number) => console.log(val);

  const tabClick = (value: any) => {
    console.log('do something with item', value);
  }


  return (
    <div className="container mt-3">
      <TabBar items={users} onTabClick={tabClick}/>

      <Panel
        title="pippo" type="success" icon="fa fa-google" marginBottom
        onIconClick={(val) => openUrl('http://www.google.com/' + val)}
      > ... 1</Panel>

      <Panel
        title="pluto" type="failed" icon="fa fa-linkedin"
        onIconClick={doSomething}
      >... 2</Panel>

      <Panel title="lista" icon="fa fa-link">
        <button>1</button>
        <button>2</button>
        <button>3</button>
      </Panel>
    </div>
  )
}
