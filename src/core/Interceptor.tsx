import React, { useEffect } from 'react';
import axios, { AxiosRequestConfig } from 'axios';

export const Interceptor: React.FC = () => {
  useEffect(() => {
    // Add a request interceptor
    axios.interceptors.request.use(function (config) {
      // Do something before request is sent
      // console.log('log', config)
      const newConfig: AxiosRequestConfig = {
        ...config,
        headers: {
          APIKEY: '123'
        }
      }
      return newConfig;
    }, function (error) {
      // Do something with request error
      return Promise.reject(error);
    });

// Add a response interceptor
    axios.interceptors.response.use(function (response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function (error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      return Promise.reject(error);
    });
  }, [])
  return <></>
};
