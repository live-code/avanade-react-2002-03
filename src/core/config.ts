import axios from 'axios';

export function getConfig() {
  return axios.get('http://localhost:3001/config')
    .then(res => res.data)
}
