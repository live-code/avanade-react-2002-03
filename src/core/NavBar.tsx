import React, { useContext, useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import classNames from 'classnames';
import { useMediaQuery } from '@react-hook/media-query';
import { AppContext, useStore } from '../App';

export const NavBar: React.FC = () => {
  const [isOpened, setIsOpened] = useState(false)
  const state = useContext(AppContext)
  const name = useStore(s => s.name)

  const isLarge = useMediaQuery('only screen and (min-width: 568px)')

  useEffect(() => {
    if (!isLarge) {
      setIsOpened(false)
    }
  }, [isLarge])
/*
  function getActive(state: { isActive: boolean }) {
    return state.isActive ? { color: 'red' } : {}
  }*/

  function getActive({ isActive }: { isActive: boolean } ) {
    return isActive ? 'active nav-link' : 'nav-link';
  }

  return <div>
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand">Navbar {state?.auth?.name} - {name}</a>
        <button
          onClick={() => setIsOpened(s => !s)}
          className="navbar-toggler " type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={classNames('collapse navbar-collapse', {show: isOpened})} id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink
                className={getActive}
                to="login"
                onClick={() => setIsOpened(false)}>login</NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className={getActive}
                to="context"
                onClick={() => setIsOpened(false)}>context</NavLink>
            </li>

            <li className="nav-item">
              <NavLink
                className={getActive}
                to="catalog"
                onClick={() => setIsOpened(false)}>catalog</NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className={getActive}
                to="products"
                onClick={() => setIsOpened(false)}>products</NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="demo1"
                className={getActive}
                onClick={() => setIsOpened(false)}>demo 1</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="demo2"
                       className={getActive}
                       onClick={() => setIsOpened(false)}>demo 2</NavLink>
            </li>

          </ul>
        </div>
      </div>
    </nav>
  </div>
};
